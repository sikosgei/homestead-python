scp -r HomeSteadAPI/ root@138.68.184.32:~/

sudo apt-get install python3-venv
python3 -m venv venv
pip install -r requirements.txt


cd /etc/systemd/system/

sudo systemctl deamon-reload
sudo systemctl start homestead_fastapi.service
sudo systemctl enable homestead_fastapi.service

journalctl -uf homestead_fastapi.service 

journalctl -xefu homestead_fastapi.service 