from fastapi import APIRouter
from app.core.config import settings
from app.api.routes.users_routes import user_router
from app.api.routes.vendor_routes import vendor_router
from app.api.routes.service_routes import service_router
from app.api.routes.expense_routes import expense_router
from app.api.routes.permissions_routes import permisions_router
from app.api.routes.expense_category_routes import expense_category_router
from app.api.routes.internet_package_routes import internet_package_router

api_router = APIRouter(prefix="/api", tags=["API"])

api_router.include_router(user_router)
api_router.include_router(vendor_router)
api_router.include_router(service_router)
api_router.include_router(expense_router)
api_router.include_router(internet_package_router)
api_router.include_router(expense_category_router)
api_router.include_router(permisions_router)


@api_router.get(path="/healthcheck")
def api_health_check():
    return {"status": "OK"}


@api_router.get(path="/allowed_origins")
def allowed_origins():
    return {"allowed_origins": settings.allowed_origins}
