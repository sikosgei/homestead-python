from typing import Optional
from pydantic import BaseModel, EmailStr


class VendorQuery(BaseModel):
    email: EmailStr

    class config:
        json_schema_extra = {
            "example": {
                "email": "email@example.com",
            }
        }


class VendorBase(BaseModel):
    email: EmailStr
    name: str
    description: str
    phone_number: str
    payment_method: str
    account_name: str
    account_number: str
    status: int
    user_id: int


class VendorCreate(VendorBase):
    class config:
        json_schema_extra = {
            "example": {
                "email": "email@example.com",
                "name": "vendor_name",
                "description": "vendor_description",
                "phone_number": "+1234567890",
                "payment_method": "payment_method",
                "account_number": "account_number",
                "account_name": "account_name",
                "status": 1,
                "user_id": 1,
            }
        }


class VendorRead(VendorBase):
    vendor_id: int

    class config:
        json_schema_extra = {
            "example": {
                "vendor_id": 1,
                "email": "email@example.com",
                "name": "vendor_name",
                "description": "vendor_description",
                "phone_number": "+1234567890",
                "payment_method": "payment_method",
                "account_number": "account_number",
                "account_name": "account_name",
                "status": 1,
                "user_id": 1,
            }
        }


class VendorUpdate(VendorBase):
    vendor_id: int
    name: Optional[str] = None
    description: Optional[str] = None
    phone_number: Optional[str] = None
    payment_method: Optional[str] = None
    account_name: Optional[str] = None
    account_number: Optional[str] = None
    status: Optional[int] = None
    user_id: Optional[int] = None

    class config:
        json_schema_extra = {
            "example": {
                "vendor_id": 1,
                "name": "vendor_name",
                "description": "vendor_description",
                "email": "email@example.com",
                "phone_number": "+1234567890",
                "payment_method": "payment_method",
                "account_number": "account_number",
                "account_name": "account_name",
                "status": 1,
                "user_id": 1,
            }
        }
