from typing import Optional
from pydantic import BaseModel


class InternetPackageBase(BaseModel):
    name: str
    description: str
    price: float
    volume: float
    user_id: int


class InternetPackageQuery(BaseModel):
    name: str

    class Config:
        json_schema_extra = {
            "example": {
                "name": "internet_package_name",
            }
        }


class InternetPackageCreate(InternetPackageBase):
    class Config:
        json_schema_extra = {
            "example": {
                "name": "internet_package_name",
                "description": "description",
                "price": 0.0,
                "volume": 0.0,
                "user_id": 1,
            }
        }


class InternetPackageRead(InternetPackageBase):
    internet_package_id: int

    class Config:
        json_schema_extra = {
            "example": {
                "name": "internet_package_name",
                "description": "description",
                "internet_package_id": 1,
                "price": 0.0,
                "volume": 0.0,
                "user_id": 1,
            }
        }


class InternetPackageUpdate(InternetPackageBase):
    internet_package_id: int
    price: Optional[float] = None
    volume: Optional[float] = None
    user_id: Optional[int] = None

    class Config:
        json_schema_extra = {
            "example": {
                "name": "internet_package_name",
                "description": "description",
                "internet_package_id": 1,
                "price": 0.0,
                "volume": 0.0,
                "user_id": 1,
            }
        }
