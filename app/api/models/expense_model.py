from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class ExpenseQuery(BaseModel):
    name: str

    class Config:
        json_schema_extra = {
            "example": {
                "name": "expense_name",
            }
        }


class ExpenseBase(BaseModel):
    name: str
    description: str
    amount_due: float
    amount_paid: float
    date_incurred: datetime
    status: int
    user_id: int
    vendor_id: int
    expense_category_id: int


class ExpenseCreate(ExpenseBase):
    class Config:
        json_schema_extra = {
            "example": {
                "name": "expense_name",
                "description": "description",
                "amount_due": 0.0,
                "amount_paid": 0.0,
                "date_incurred": "2023-10-28T00:00:00",
                "status": 1,
                "user_id": 1,
                "vendor_id": 1,
                "expense_category_id": 1,
            }
        }


class ExpenseRead(ExpenseBase):
    expense_id: int

    class Config:
        json_schema_extra = {
            "example": {
                "expense_id": 1,
                "name": "expense_name",
                "description": "description",
                "amount_due": 0.0,
                "amount_paid": 0.0,
                "date_incurred": "2023-10-28T00:00:00",
                "status": 1,
                "user_id": 1,
                "vendor_id": 1,
                "expense_category_id": 1,
            }
        }


class ExpenseUpdate(ExpenseBase):
    expense_id: int
    name: Optional[str] = None
    description: Optional[str] = None
    amount_due: float
    amount_paid: float
    date_incurred: datetime
    status: Optional[int] = None
    user_id: Optional[int] = None
    vendor_id: Optional[int] = None
    expense_category_id: Optional[int] = None

    class Config:
        json_schema_extra = {
            "example": {
                "expense_id": 1,
                "name": "expense_name",
                "description": "description",
                "amount_due": 0.0,
                "amount_paid": 0.0,
                "date_incurred": "2023-10-28T00:00:00",
                "status": 1,
                "user_id": 1,
                "vendor_id": 1,
                "expense_category_id": 1,
            }
        }
