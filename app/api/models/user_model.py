from typing import Optional, List
from pydantic import BaseModel, EmailStr
from app.api.utils.enums import Gender, UserRole, AccountStatus


class UserQuery(BaseModel):
    user_name: EmailStr

    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
            }
        }


class AuthenticateUser(UserQuery):
    password: str

    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
                "password": "<PASSWORD>",
            }
        }


class UpdateUserPassword(AuthenticateUser):
    pass


class UpdateUserStatus(UserQuery):
    account_status: AccountStatus

    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
                "account_status": "pending",
            }
        }


class AccessToken(BaseModel):
    access_token: str
    token_type: str = "bearer"


class BaseUser(BaseModel):
    password: str
    full_name: str
    gender: Gender
    shares: int = 0
    balance: int = 0
    notified: int = 0
    user_name: EmailStr
    loan_amount: int = 0
    primary_phone_number: str
    permissions_group_id: int = 1
    photo: Optional[str] = None
    address: Optional[str] = None
    user_role: UserRole = UserRole.USER
    secondary_phone_number: Optional[str] = None
    national_identification_number: Optional[str] = None
    account_status: AccountStatus = AccountStatus.PENDING


class CreateUSer(BaseUser):
    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
                "password": "<PASSWORD>",
                "permissions_group_id": 1,
                "full_name": "John Doe",
                "gender": "male",
                "shares": 0,
                "balance": 0,
                "notified": 0,
                "loan_amount": 0,
                "primary_phone_number": "1234567890",
                "photo": "null",
                "address": "null",
                "user_role": "user",
                "secondary_phone_number": "null",
                "national_identification_number": "null",
                "account_status": "pending",
            },
        }


class ReadUser(BaseUser):
    user_id: int
    permissions: List[str]

    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
                "password": "<HASHED_PASSWORD>",
                "permissions_group_id": 1,
                "full_name": "John Doe",
                "gender": "male",
                "shares": 0,
                "balance": 0,
                "notified": 0,
                "loan_amount": 0,
                "primary_phone_number": "1234567890",
                "photo": "null",
                "address": "null",
                "user_role": "user",
                "secondary_phone_number": "null",
                "national_identification_number": "null",
                "account_status": "pending",
                "user_id": 0,
                "permissions": [],
            }
        }


class UpdateUser(BaseUser):
    password: Optional[str] = None
    full_name: Optional[str] = None
    gender: Optional[Gender] = None
    shares: Optional[int] = None
    balance: Optional[int] = None
    notified: Optional[int] = None
    loan_amount: Optional[int] = None
    primary_phone_number: Optional[str] = None
    photo: Optional[str] = None
    address: Optional[str] = None
    user_role: Optional[UserRole] = None
    secondary_phone_number: Optional[str] = None
    national_identification_number: Optional[str] = None
    account_status: Optional[AccountStatus] = None

    class Config:
        json_schema_extra = {
            "example": {
                "user_name": "test_email@email.com",
                "password": "<PASSWORD>",
                "permissions_group_id": 1,
                "full_name": "John Doe",
                "gender": "male",
                "shares": 0,
                "balance": 0,
                "notified": 0,
                "loan_amount": 0,
                "primary_phone_number": "1234567890",
                "photo": "null",
                "address": "null",
                "user_role": "user",
                "secondary_phone_number": "null",
                "national_identification_number": "null",
                "account_status": "pending",
            }
        }
