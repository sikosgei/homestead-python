from typing import Optional
from pydantic import BaseModel


class ServiceBase(BaseModel):
    name: str
    description: str
    price: float
    status: str
    user_id: int


class ServiceQuery(BaseModel):
    name: str

    class Config:
        json_schema_extra = {
            "example": {
                "name": "service_name",
            }
        }


class ServiceCreate(ServiceBase):
    class Config:
        json_schema_extra = {
            "example": {
                "name": "service_name",
                "description": "description",
                "price": 0.0,
                "status": "active",
                "user_id": 1,
            }
        }


class ServiceRead(ServiceBase):
    service_id: int

    class Config:
        json_schema_extra = {
            "example": {
                "service_id": 1,
                "name": "service_name",
                "description": "description",
                "price": 0.0,
                "status": "active",
                "user_id": 1,
            }
        }


class ServiceUpdate(ServiceBase):
    service_id: int
    price: Optional[float] = None
    status: Optional[str] = None
    user_id: Optional[int] = None

    class Config:
        json_schema_extra = {
            "example": {
                "service_id": 1,
                "name": "service_name",
                "description": "description",
                "price": 0.0,
                "status": "active",
                "user_id": 1,
            }
        }
