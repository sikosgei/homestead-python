from typing import Optional
from pydantic import BaseModel


class ExpenseCategoryQuery(BaseModel):
    name: str

    class Config:
        json_schema_extra = {
            "example": {
                "name": "expense_category_name",
            }
        }


class ExpenseCategoryBase(BaseModel):
    name: str
    description: str
    status: int
    user_id: int


class ExpenseCategoryCreate(ExpenseCategoryBase):
    class Config:
        json_schema_extra = {
            "example": {
                "name": "expense_category_name",
                "description": "description",
                "status": 1,
                "user_id": 1,
            }
        }


class ExpenseCategoryRead(ExpenseCategoryBase):
    expense_category_id: int

    class Config:
        json_schema_extra = {
            "example": {
                "expense_category_id": 1,
                "name": "expense_category_name",
                "description": "description",
                "status": 1,
                "user_id": 1,
            }
        }


class ExpenseCategoryUpdate(ExpenseCategoryBase):
    expense_category_id: int
    name: Optional[str] = None
    description: Optional[str] = None
    status: Optional[int] = None
    user_id: Optional[int] = None

    class Config:
        json_schema_extra = {
            "example": {
                "expense_category_id": 1,
                "name": "expense_category_name",
                "description": "description",
                "status": 1,
                "user_id": 1,
            }
        }
