from sqlalchemy.orm import Session
from app.core.config import settings
from app.core.database import engine
from app.api.auth.hashing import hash_password
from app.api.utils.permissions import get_admin_permissions
from app.api.utils.enums import Gender, UserRole, AccountStatus
from app.api.schema.db_schema import UserSchema, PermissionGroupSchema


def create_permission_groups() -> int:
    with Session(engine) as session:
        permission_group_in_db = (
            session.query(PermissionGroupSchema)
            .filter(PermissionGroupSchema.permission_group_name == "super_user")
            .one_or_none()
        )

        if permission_group_in_db:
            return permission_group_in_db.permission_group_id

        admin_permissions = get_admin_permissions()
        admin_permission_group = PermissionGroupSchema(
            permission_list=admin_permissions, permission_group_name="super_user"
        )

        session.add(admin_permission_group)
        session.commit()
        session.refresh(admin_permission_group)

    return admin_permission_group.permission_group_id


def create_super_user(permissions_group_id: int) -> int:
    with Session(engine) as session:
        # check if super_user exists
        user_in_db = (
            session.query(UserSchema)
            .filter(UserSchema.user_name == settings.super_user_username)
            .one_or_none()
        )

        if user_in_db:
            return user_in_db.user_id

        super_user = UserSchema(
            user_name=settings.super_user_username,
            password=hash_password(settings.super_user_password),
            full_name="Sila Kosgei",
            primary_phone_number="+254721789990",
            user_role=UserRole.ADMIN,
            gender=Gender.MALE,
            account_status=AccountStatus.ACTIVE,
            permissions_group_id=permissions_group_id,
        )

        session.add(super_user)
        session.commit()
        session.refresh(super_user)

    return super_user.user_id


def super_user_initialization() -> int:
    permissions_group_id = create_permission_groups()
    super_user_id = create_super_user(permissions_group_id=permissions_group_id)

    return super_user_id
