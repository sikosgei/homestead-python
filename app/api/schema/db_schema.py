from datetime import datetime
from pydantic import EmailStr
from typing import Optional, List
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship
from app.api.utils.enums import Gender, UserRole, AccountStatus, RecurringExpensePeriod
from sqlalchemy import (
    Integer,
    String,
    ForeignKey,
    Text,
    DateTime,
    Double,
    Enum,
    JSON,
)


class Base(DeclarativeBase):
    pass


class TimestampMixin:
    created_at: Mapped[datetime] = mapped_column(
        DateTime, default=datetime.now, nullable=False
    )

    updated_at: Mapped[datetime] = mapped_column(
        DateTime, default=datetime.now, onupdate=datetime.now, nullable=False
    )


class NameAndDescriptionMixin:
    name: Mapped[str] = mapped_column(String(length=200), nullable=False, unique=True)
    description: Mapped[str] = mapped_column(Text, nullable=False)


class UserSchema(TimestampMixin, Base):
    __tablename__ = "Users"

    user_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    user_name: Mapped[EmailStr] = mapped_column(
        String(length=100), nullable=False, unique=True, index=True
    )
    password: Mapped[str] = mapped_column(String(length=100), nullable=False)

    full_name: Mapped[str] = mapped_column(String(length=100), nullable=False)
    gender: Mapped[Gender] = mapped_column(Enum(Gender), nullable=False)
    account_status: Mapped[AccountStatus] = mapped_column(
        Enum(AccountStatus), nullable=False, default=AccountStatus.PENDING
    )
    primary_phone_number: Mapped[str] = mapped_column(String(length=20), nullable=False)
    secondary_phone_number: Mapped[Optional[str]] = mapped_column(
        String(length=15), nullable=True
    )

    notified: Mapped[int] = mapped_column(Integer, default=0)
    shares: Mapped[int] = mapped_column(Integer, default=0)
    balance: Mapped[float] = mapped_column(Double, default=0)
    loan_amount: Mapped[float] = mapped_column(Double, default=0)
    national_identification_number: Mapped[str] = mapped_column(
        String(length=20), nullable=True
    )
    photo: Mapped[str] = mapped_column(String(length=70), nullable=True)
    address: Mapped[str] = mapped_column(String(length=70), nullable=True)
    user_role: Mapped[UserRole] = mapped_column(Enum(UserRole), default=UserRole.USER)
    email_verified_at: Mapped[Optional[datetime]]

    ################# Foreign Keys definitions
    permissions_group_id: Mapped[int] = mapped_column(
        ForeignKey("PermissionGroups.permission_group_id")
    )

    ################# Relationship Definitions

    permissions: Mapped[Optional["PermissionGroupSchema"]] = relationship(
        back_populates="user"
    )
    document_types: Mapped[Optional[List["DocumentTypeSchema"]]] = relationship(
        back_populates="user"
    )

    services: Mapped[Optional[List["ServicesSchema"]]] = relationship(
        back_populates="user"
    )

    vendors: Mapped[Optional[List["VendorsSchema"]]] = relationship(
        back_populates="user"
    )
    expense_category: Mapped[Optional[List["ExpenseCategorySchema"]]] = relationship(
        back_populates="user"
    )

    expenses: Mapped[Optional[List["ExpensesSchema"]]] = relationship(
        back_populates="user"
    )
    recurring_expenses: Mapped[
        Optional[List["RecurringExpensesSchema"]]
    ] = relationship(back_populates="user")

    internet_packages: Mapped[Optional[List["InternetPackagesSchema"]]] = relationship(
        back_populates="user"
    )

    # class representation dunder method
    def __repr__(self):
        return f"<User(id={self.user_id}, email_address={self.user_name})>"


class PermissionGroupSchema(TimestampMixin, Base):
    __tablename__ = "PermissionGroups"

    permission_group_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    permission_list: Mapped[List[str]] = mapped_column(JSON, nullable=False)
    permission_group_name: Mapped[str] = mapped_column(
        String(100), nullable=False, unique=True
    )

    # foreign key
    # user_id: Mapped[Optional[int]] = mapped_column(ForeignKey("Users.user_id"))

    user: Mapped[Optional["UserSchema"]] = relationship(back_populates="permissions")


class ServicesSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "Services"

    service_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    price: Mapped[float] = mapped_column(Double, nullable=False)
    status: Mapped[str] = mapped_column(String(length=50), default="active")

    # foreign key
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))

    # relationship
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="services", uselist=False
    )


class InternetPackagesSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "InternetPackages"

    internet_package_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    price: Mapped[float] = mapped_column(Double, nullable=False)
    volume: Mapped[float] = mapped_column(Double, nullable=False)

    # foreign key
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))

    # relationship
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="internet_packages", uselist=False
    )


class DocumentTypeSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "DocumentTypes"

    document_type_id: Mapped[int] = mapped_column(Integer, primary_key=True)

    # foreign key
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))

    # relationship
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="document_types", uselist=False
    )


class VendorsSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "Vendors"

    vendor_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    email: Mapped[EmailStr] = mapped_column(String(length=200), nullable=False)
    phone_number: Mapped[str] = mapped_column(String(length=200), nullable=False)
    payment_method: Mapped[str] = mapped_column(String(length=255), nullable=False)
    account_name: Mapped[str] = mapped_column(String(length=255), nullable=False)
    account_number: Mapped[str] = mapped_column(String(length=255), nullable=False)
    status: Mapped[int] = mapped_column(Integer, nullable=False, default=1)

    # foreign key(s)
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))

    # relationship
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="vendors", uselist=False
    )


class ExpenseCategorySchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "ExpenseCategory"

    expense_category_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    status: Mapped[int] = mapped_column(Integer, nullable=False, default=1)

    # foreign key(s)
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))

    # relationship(s)
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="expense_category", uselist=False
    )


class RecurringExpensesSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "RecurringExpenses"

    recurring_expense_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    amount: Mapped[float] = mapped_column(Double, nullable=False)
    recurring_period: Mapped[RecurringExpensePeriod] = mapped_column(
        Enum(RecurringExpensePeriod), nullable=False
    )
    recurring_date: Mapped[datetime] = mapped_column(DateTime, nullable=False)
    status: Mapped[int] = mapped_column(Integer, nullable=False, default=1)

    # foreign key(s)
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))
    vendor_id: Mapped[int] = mapped_column(ForeignKey("Vendors.vendor_id"))
    expense_category_id: Mapped[int] = mapped_column(
        ForeignKey("ExpenseCategory.expense_category_id")
    )

    # relationship(s)
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="recurring_expenses", uselist=False
    )


class ExpensesSchema(NameAndDescriptionMixin, TimestampMixin, Base):
    __tablename__ = "Expenses"

    expense_id: Mapped[int] = mapped_column(Integer, primary_key=True)
    amount_due: Mapped[float] = mapped_column(Double, nullable=False, default=0.0)
    amount_paid: Mapped[float] = mapped_column(Double, nullable=False, default=0.0)
    status: Mapped[int] = mapped_column(Integer, nullable=False, default=1)
    date_incurred: Mapped[datetime] = mapped_column(DateTime, nullable=False)

    # foreign keys
    user_id: Mapped[int] = mapped_column(ForeignKey("Users.user_id"))
    vendor_id: Mapped[int] = mapped_column(ForeignKey("Vendors.vendor_id"))
    expense_category_id: Mapped[int] = mapped_column(
        ForeignKey("ExpenseCategory.expense_category_id")
    )

    # relationship(s)
    user: Mapped[Optional["UserSchema"]] = relationship(
        back_populates="expenses", uselist=False
    )
