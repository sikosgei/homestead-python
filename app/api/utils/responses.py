from fastapi import Response


class AppResponses:
    def user_not_found() -> Response:
        return Response(status_code=404, content="User Not Found")

    def internet_package_not_found() -> Response:
        return Response(status_code=404, content="Internet Package Not Found")

    def internet_package_already_exists() -> Response:
        return Response(status_code=409, content="Internet Package Already Exists")

    def service_not_found() -> Response:
        return Response(status_code=404, content="Service Not Found")

    def service_already_exists() -> Response:
        return Response(status_code=409, content="Service Already Exists")

    def account_type_not_found() -> Response:
        return Response(status_code=404, content="Account Type Not Found")

    def account_type_already_exists() -> Response:
        return Response(status_code=409, content="Account Type Already Exists")

    def expense_already_exists() -> Response:
        return Response(status_code=409, content="Expense Already Exists")

    def expense_not_found() -> Response:
        return Response(status_code=404, content="Expense Not Found")

    def expense_category_already_exists() -> Response:
        return Response(status_code=409, content="Expense Category Already Exists")

    def expense_category_not_found() -> Response:
        return Response(status_code=404, content="Expense Category Not Found")

    def vendor_not_found() -> Response:
        return Response(status_code=404, content="Vendor Not Found")

    def vendor_already_exists() -> Response:
        return Response(status_code=409, content="Vendor Already Exists")

    def user_already_exists() -> Response:
        return Response(status_code=409, content="User Already Exists")

    def incorrect_user_details() -> Response:
        return Response(status_code=401, content="Incorrect Username or Password")

    def user_not_authorized() -> Response:
        return Response(status_code=403, content="Not Authorized")

    def credentials_validation_error() -> Response:
        return Response(
            status_code=401,
            content="Cound Not Validate Credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
