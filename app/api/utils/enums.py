from enum import Enum


class UserRole(str, Enum):
    ADMIN = "admin"
    SUBSCRIBER = "subscriber"
    MEMBER = "member"
    USER = "user"

    def __str__(self):
        return self.value


class Gender(str, Enum):
    MALE = "male"
    FEMALE = "female"
    OTHER = "other"

    def __str__(self):
        return self.value


class AccountStatus(str, Enum):
    PENDING = "pending"
    ACTIVE = "active"
    SUSPENDED = "suspended"
    EXPELLED = "expelled"
    REJECTED = "rejected"

    def __str__(self) -> str:
        return self.value


class RecurringExpensePeriod(str, Enum):
    DAILY = "daily"
    WEEKLY = "weekly"
    BI_WEEKLY = "bi_weekly"
    MONTHLY = "monthly"
    ANUALLY = "anually"


class BillingPermission(str, Enum):
    LIST_ANY = "list_any"
    GET_ANY = "get_any"

    def __str__(self) -> str:
        return self.value


class ConfigPermission(str, Enum):
    SERVICES = "services"
    DEPENDANT_TYPE = "dependent_type"
    ADD_DEPENDANT_TYPE = "dependent_type.add_dependent_type"
    LIST_DEPENDANT_TYPE = "dependent_type.list_dependent_type"
    REGISTRATION_FEE = "registration_fee"
    ADD_REGISTRATION_FEE = "registration_fee.add_registration_fee"
    LIST_REGISTRATION_FEE = "registration_fee.list_registration_fee"
    MANAGE_COST_OF_SHARES = "manage_cost_of_shares"
    MANAGE_LOAN_CONFIGS = "manage_loan_configs"
    PAYMENT_ACCOUNTS = "payment_accounts"
    ADD_PAYMENT_ACCOUNT = "payment_accounts.add_payment_account"
    LIST_PAYMENT_ACCOUNTS = "payment_accounts.list_payment_accounts"
    UPDATE_PAYMENT_ACCOUNTS = "payment_accounts.update_payment_account_status"
    CORE_SETTINGS = "core_settings"
    NOTIFICATION_MESSAGES = "notification_messages"
    DOCUMENT_TYPES = "document_types"
    ADD_DOCUMENT_TYPE = "document_types.add_document_type"
    LIST_DOCUMENT_TYPES = "document_types.list_document_types"
    SHARE_CAPITALS = "share_capitals"
    ADD_SHARE_CAPITAL = "share_capitals.add_share_capital"
    LIST_SHARE_CAPITALS = "share_capitals.list_share_capitals"
    LOG_TYPES = "log_types"
    ACCOUNT_TYPES = "account_types"
    ADD_ACCOUNT_TYPE = "account_types.add_account_type"
    DEPARTMENTS = "departments"
    ADD_DEPARTMENT = "departments.add_department"
    LIST_DEPARTMENTS = "departments.list_departments"
    DEPARTMENTS_VIEW = "departments.view_department"
    ADD_DEPARTMENTS_VIEW = "departments.view_department.add_module"
    LIST_DEPARTMENTS_VIEW = "departments.view_department.list_modules"

    def __str__(self) -> str:
        return self.value


class DocumentPermissions(str, Enum):
    STORE_DOCUMENT = "store_document"
    LIST_DOCUMENTS = "list_documents"
    UPLOAD_PROFILE = "upload-profile"

    def __str__(self) -> str:
        return self.value


class DashboardPermissions(str, Enum):
    DASHBOARD = "dashboard"

    def __str__(self) -> str:
        return self.value


class DepartmentPermissions(str, Enum):
    ADD_DEPARTMENT = "add_department"
    LIST_DEPARTMENTS = "list_departments"
    VIEW_DEPARTMENT = "view_department"
    VIEW_DEPARTMENT_ADD_MODULE = "view_department.add_module"
    VIEW_DEPARTMENT_LIST_MODULES = "view_department.list_modules"

    def __str__(self) -> str:
        return self.value


class ExpensesPermissions(str, Enum):
    ADD_EXPENSE = "add_expense"
    LIST_EXPENSES = "list_expenses"
    DISCARD_EXPENSE = "discard"
    VENDORS = "vendors"
    ADD_VENDOR = "vendors.add_vendor"
    LIST_VENDORS = "vendors.list_vendors"
    CATEGORIES = "categories"
    ADD_CATEGORY = "categories.add_category"
    LIST_CATEGORIES = "categories.list_categories"
    PAYMENTS = "payments"
    ADD_PAYMENT = "payments.add_payment"
    LIST_PAYMENTS = "payments.list_payments"
    RECURRING = "recurring"
    ADD_RECCURING = "recurring.add_recurring"
    LIST_RECURRING = "recurring.list_recurring"

    def __str__(self) -> str:
        return self.value


class InternetPackagesPermissions(str, Enum):
    INTERNET_PACKAGES = "internet_packages"
    ADD_INTERNET_PACKAGES = "add_package"
    LIST_INTENET_PACKAGES = "list_packages"
    GET_SUBSCRIBERS = "get_subscribers"

    def __str__(self) -> str:
        return self.value


class MpesaPermissions(str, Enum):
    LIST_TRANSACTIONS = "list_transactions"
    DISCARD_TRANSACTION = "discard_transaction"
    ASSIGN_CONTRIBUTIONS = "assign_contributions"
    ASSIGN_PAY_LOAN_CONTRIBUTION = "assign_contributions.pay-loan"
    ASSIGN_UNALLOCATED_CONTRIBUTION = "assign_contributions.un-allocate"
    REASSIGN_CONTRIBUTIONS = "assign_contributions.re-assign"

    def __str__(self) -> str:
        return self.value


class MessagesPermissions(str, Enum):
    LIST_ALL = "list_all"

    def __str__(self) -> str:
        return self.value


class PaymentPermissions(str, Enum):
    STORE = "store"
    LIST_ANY = "list_any"
    GET_ANY = "get_any"

    def __str__(self) -> str:
        return self.value
