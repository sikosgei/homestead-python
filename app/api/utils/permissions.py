from typing import List
from app.api.utils.enums import (
    BillingPermission,
    ConfigPermission,
    DashboardPermissions,
    DepartmentPermissions,
    DocumentPermissions,
    ExpensesPermissions,
    InternetPackagesPermissions,
    MessagesPermissions,
    MpesaPermissions,
    PaymentPermissions,
)


def get_admin_permissions() -> List[str]:
    all_permissions = get_all_permisions()
    final_list = []
    for permission in all_permissions:
        final_list.extend(all_permissions[permission])

    return final_list


def get_all_permisions() -> dict:
    return {
        "billing": [permission.value for permission in BillingPermission],
        "config": [permission.value for permission in ConfigPermission],
        "dashboard": [permission.value for permission in DashboardPermissions],
        "department": [permission.value for permission in DepartmentPermissions],
        "documents": [permission.value for permission in DocumentPermissions],
        "expenses": [permission.value for permission in ExpensesPermissions],
        "internet_packages": [
            permission.value for permission in InternetPackagesPermissions
        ],
        "messages": [permission.value for permission in MessagesPermissions],
        "mpesa": [permission.value for permission in MpesaPermissions],
        "payments": [permission.value for permission in PaymentPermissions],
    }
