from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from app.core.database import get_database_session
from fastapi.security import OAuth2PasswordBearer
from app.api.controllers.user_controller import (
    create_user,
    get_users,
    get_user_instance,
    update_user_instance,
    delete_user_instance,
    update_user_password,
    update_user_status,
)
from app.api.models.user_model import (
    CreateUSer,
    ReadUser,
    UpdateUser,
    UpdateUserStatus,
    UserQuery,
    UpdateUserPassword,
)

user_router = APIRouter(prefix="/user", tags=["Users"])

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@user_router.post(path="/create", response_model=ReadUser)
def create_new_user(
    *,
    session: Session = Depends(get_database_session),
    user: CreateUSer,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_user(session=session, new_user=user, token=token)


@user_router.get(path="/all", response_model=list[ReadUser])
def get_all_users(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_user_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_users(
        session=session, token=token, limit=limit, starting_user_id=starting_user_id
    )


@user_router.post(path="/read", response_model=ReadUser)
def get_user(
    *,
    session: Session = Depends(get_database_session),
    user_query: UserQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_user_instance(session=session, user_query=user_query, token=token)


@user_router.post(path="/update_password", response_model=UpdateUserPassword)
def update_user_account_password(
    *,
    session: Session = Depends(get_database_session),
    new_user_password: UpdateUserPassword,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_user_password(session=session, new_user_password=new_user_password, token=token)


@user_router.post(path="/update_user_status", response_model=UpdateUserStatus)
def update_user_account_status(
    *,
    session: Session = Depends(get_database_session),
    new_user_status: UpdateUserStatus,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_user_status(session=session, new_user_status=new_user_status, token=token)


@user_router.post(path="/update", response_model=ReadUser)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    user_update: UpdateUser,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_user_instance(session=session, user_update=user_update, token=token)


@user_router.delete(path="/delete", response_model=UserQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    user_query: UserQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_user_instance(session=session, user_query=user_query, token=token)
