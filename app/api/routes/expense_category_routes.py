from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from fastapi.security import OAuth2PasswordBearer
from app.core.database import get_database_session
from app.api.controllers.expense_category_controller import (
    create_expense_category_instance,
    update_expense_category_instance,
    delete_expense_category_instance,
    get_all_expense_categories_from_db,
)
from app.api.models.expense_category_model import (
    ExpenseCategoryCreate,
    ExpenseCategoryRead,
    ExpenseCategoryUpdate,
    ExpenseCategoryQuery,
)

expense_category_router = APIRouter(
    prefix="/expense_category", tags=["ExpenseCategory"]
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@expense_category_router.post(path="/create", response_model=ExpenseCategoryRead)
def create_expense_category(
    *,
    session: Session = Depends(get_database_session),
    new_expense_category: ExpenseCategoryCreate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_expense_category_instance(
        session=session, new_expense_category=new_expense_category, token=token
    )


@expense_category_router.get(path="/all", response_model=list[ExpenseCategoryRead])
def get_all_expense_categories(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_expense_category_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_all_expense_categories_from_db(
        session=session,
        token=token,
        limit=limit,
        starting_expense_category_id=starting_expense_category_id,
    )


@expense_category_router.post(path="/update", response_model=ExpenseCategoryRead)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    expense_category_update: ExpenseCategoryUpdate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_expense_category_instance(
        session=session, expense_category_update=expense_category_update, token=token
    )


@expense_category_router.delete(path="/delete", response_model=ExpenseCategoryQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    expense_category_query: ExpenseCategoryQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_expense_category_instance(
        session=session, expense_category_query=expense_category_query, token=token
    )
