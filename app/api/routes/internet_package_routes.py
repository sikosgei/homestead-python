from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from fastapi.security import OAuth2PasswordBearer
from app.core.database import get_database_session
from app.api.controllers.internet_package_controller import (
    create_internet_package_instance,
    update_internet_package_instance,
    delete_internet_package_instance,
    get_all_internet_packages_from_db,
)
from app.api.models.internet_package_model import (
    InternetPackageCreate,
    InternetPackageRead,
    InternetPackageUpdate,
    InternetPackageQuery,
)

internet_package_router = APIRouter(
    prefix="/internet_packages", tags=["InternetPackage"]
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@internet_package_router.post(path="/create", response_model=InternetPackageRead)
def create_internet_package(
    *,
    session: Session = Depends(get_database_session),
    new_internet_package: InternetPackageCreate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_internet_package_instance(
        session=session, new_internet_package=new_internet_package, token=token
    )


@internet_package_router.get(path="/all", response_model=list[InternetPackageRead])
def get_all_internet_packages(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_internet_package_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_all_internet_packages_from_db(
        session=session,
        token=token,
        limit=limit,
        starting_internet_package_id=starting_internet_package_id,
    )


@internet_package_router.post(path="/update", response_model=InternetPackageRead)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    internet_package_update: InternetPackageUpdate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_internet_package_instance(
        session=session, internet_package_update=internet_package_update, token=token
    )


@internet_package_router.delete(path="/delete", response_model=InternetPackageQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    internet_package_query: InternetPackageQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_internet_package_instance(
        session=session, internet_package_query=internet_package_query, token=token
    )
