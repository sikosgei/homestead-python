from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from fastapi.security import OAuth2PasswordBearer
from app.core.database import get_database_session
from app.api.controllers.expense_controller import (
    create_expense_instance,
    update_expense_instance,
    delete_expense_instance,
    get_all_expenses_from_db,
)
from app.api.models.expense_model import (
    ExpenseCreate,
    ExpenseRead,
    ExpenseUpdate,
    ExpenseQuery,
)

expense_router = APIRouter(
    prefix="/expenses", tags=["Expense"]
)

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@expense_router.post(path="/create", response_model=ExpenseRead)
def create_expense(
    *,
    session: Session = Depends(get_database_session),
    new_expense: ExpenseCreate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_expense_instance(
        session=session, new_expense=new_expense, token=token
    )


@expense_router.get(path="/all", response_model=list[ExpenseRead])
def get_all_expenses(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_expense_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_all_expenses_from_db(
        session=session,
        token=token,
        limit=limit,
        starting_expense_id=starting_expense_id,
    )


@expense_router.post(path="/update", response_model=ExpenseRead)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    expense_update: ExpenseUpdate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_expense_instance(
        session=session, expense_update=expense_update, token=token
    )


@expense_router.delete(path="/delete", response_model=ExpenseQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    expense_query: ExpenseQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_expense_instance(
        session=session, expense_query=expense_query, token=token
    )
