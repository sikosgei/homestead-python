from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from fastapi.security import OAuth2PasswordBearer
from app.core.database import get_database_session
from app.api.controllers.vendor_controller import (
    create_vendor_instance,
    update_vendor_instance,
    delete_vendor_instance,
    get_all_vendors_from_db,
)
from app.api.models.vendor_model import (
    VendorCreate,
    VendorRead,
    VendorUpdate,
    VendorQuery,
)

vendor_router = APIRouter(prefix="/vendors", tags=["Vendor"])

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@vendor_router.post(path="/create", response_model=VendorRead)
def create_vendor(
    *,
    session: Session = Depends(get_database_session),
    new_vendor: VendorCreate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_vendor_instance(session=session, new_vendor=new_vendor, token=token)


@vendor_router.get(path="/all", response_model=list[VendorRead])
def get_all_vendors(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_vendor_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_all_vendors_from_db(
        session=session,
        token=token,
        limit=limit,
        starting_vendor_id=starting_vendor_id,
    )


@vendor_router.post(path="/update", response_model=VendorRead)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    vendor_update: VendorUpdate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_vendor_instance(
        session=session, vendor_update=vendor_update, token=token
    )


@vendor_router.delete(path="/delete", response_model=VendorQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    vendor_query: VendorQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_vendor_instance(
        session=session, vendor_query=vendor_query, token=token
    )
