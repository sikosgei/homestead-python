from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends, Query
from fastapi.security import OAuth2PasswordBearer
from app.core.database import get_database_session
from app.api.controllers.service_controller import (
    create_service_instance,
    update_service_instance,
    delete_service_instance,
    get_all_services_from_db,
)
from app.api.models.service_model import (
    ServiceCreate,
    ServiceRead,
    ServiceUpdate,
    ServiceQuery,
)

service_router = APIRouter(prefix="/services", tags=["Service"])

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


@service_router.post(path="/create", response_model=ServiceRead)
def create_service(
    *,
    session: Session = Depends(get_database_session),
    new_service: ServiceCreate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return create_service_instance(
        session=session, new_service=new_service, token=token
    )


@service_router.get(path="/all", response_model=list[ServiceRead])
def get_all_services(
    *,
    session: Session = Depends(get_database_session),
    limit: int = Query(default=10, ge=1),
    starting_service_id: int = Query(default=1, ge=1),
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return get_all_services_from_db(
        session=session,
        token=token,
        limit=limit,
        starting_service_id=starting_service_id,
    )


@service_router.post(path="/update", response_model=ServiceRead)
def update_user(
    *,
    session: Session = Depends(get_database_session),
    service_update: ServiceUpdate,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return update_service_instance(
        session=session, service_update=service_update, token=token
    )


@service_router.delete(path="/delete", response_model=ServiceQuery)
def delete_user(
    *,
    session: Session = Depends(get_database_session),
    service_query: ServiceQuery,
    token: Annotated[str, Depends(oauth2_scheme)],
):
    return delete_service_instance(
        session=session, service_query=service_query, token=token
    )
