from fastapi import APIRouter, Depends
from app.api.utils.permissions import get_all_permisions


permisions_router = APIRouter()


@permisions_router.get(path="/all_permissions")
def view_all_permisions():
    return get_all_permisions()
