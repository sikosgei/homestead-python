from typing import List
from fastapi import Response
from sqlalchemy.orm import Session
from app.api.utils.responses import AppResponses
from app.api.schema.db_schema import InternetPackagesSchema
from app.api.controllers.user_controller import get_user_from_token
from app.api.models.internet_package_model import (
    InternetPackageCreate,
    InternetPackageRead,
    InternetPackageUpdate,
    InternetPackageQuery,
)


def create_internet_package_response_model(internet_package: InternetPackagesSchema):
    return InternetPackageRead(
        internet_package_id=internet_package.internet_package_id,
        name=internet_package.name,
        description=internet_package.description,
        volume=internet_package.volume,
        price=internet_package.price,
        user_id=internet_package.user_id,
    )


def get_all_internet_packages_from_db(
    session: Session,
    token: str,
    limit: int = 10,
    starting_internet_package_id: int = 1,
) -> List[InternetPackageRead]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    internet_packages = db_results = (
        session.query(InternetPackagesSchema)
        .filter(
            InternetPackagesSchema.internet_package_id >= starting_internet_package_id
        )
        .order_by(InternetPackagesSchema.internet_package_id)
        .limit(limit)
        .all()
    )
    internet_package_read_response_models = [
        create_internet_package_response_model(internet_package=internet_package)
        for internet_package in internet_packages
    ]

    return internet_package_read_response_models


def get_internet_package_from_db(
    session: Session, internet_package_query: InternetPackageQuery
) -> InternetPackagesSchema | None:
    internet_package_from_db = (
        session.query(InternetPackagesSchema)
        .filter(InternetPackagesSchema.name == internet_package_query.name)
        .one_or_none()
    )

    if internet_package_from_db is None:
        return None

    return internet_package_from_db


def create_internet_package_instance(
    session: Session, token: str, new_internet_package: InternetPackageCreate
) -> InternetPackageRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    internet_package_query = InternetPackageQuery(name=new_internet_package.name)

    internet_package_in_db = get_internet_package_from_db(
        session=session, internet_package_query=internet_package_query
    )

    # if package already in the database return a internet package already exists Response
    if internet_package_in_db:
        return AppResponses.internet_package_already_exists()

    new_internet_package_to_insert = InternetPackagesSchema(
        name=new_internet_package.name,
        description=new_internet_package.description,
        volume=new_internet_package.volume,
        price=new_internet_package.price,
        user_id=new_internet_package.user_id,
    )

    session.add(new_internet_package_to_insert)
    session.commit()
    session.refresh(new_internet_package_to_insert)

    return create_internet_package_response_model(
        internet_package=new_internet_package_to_insert
    )


def update_internet_package_instance(
    session: Session, token: str, internet_package_update: InternetPackageUpdate
) -> InternetPackageRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    internet_package_query = InternetPackageQuery(name=internet_package_update.name)

    internet_package_in_db = get_internet_package_from_db(
        session=session, internet_package_query=internet_package_query
    )

    # if package does not exist in the database return a internet package Not Found Response
    if internet_package_in_db is None:
        return AppResponses.internet_package_not_found()

    new_internet_package_data = internet_package_update.model_dump(exclude_none=True)

    for key, value in new_internet_package_data.items():
        setattr(internet_package_in_db, key, value)

    session.add(internet_package_in_db)
    session.commit()
    session.refresh(internet_package_in_db)

    return create_internet_package_response_model(
        internet_package=internet_package_in_db
    )


def delete_internet_package_instance(
    session: Session, token: str, internet_package_query: InternetPackageQuery
) -> InternetPackageQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    internet_package_in_db = get_internet_package_from_db(
        session=session, internet_package_query=internet_package_query
    )

    # if package does not exist in the database return a internet package Not Found Response
    if internet_package_in_db is None:
        return AppResponses.internet_package_not_found()

    session.delete(internet_package_in_db)
    session.commit()

    return internet_package_query
