from typing import List
from fastapi import Response
from sqlalchemy.orm import Session
from app.api.utils.responses import AppResponses
from app.api.schema.db_schema import ExpenseCategorySchema
from app.api.controllers.user_controller import get_user_from_token
from app.api.models.expense_category_model import (
    ExpenseCategoryCreate,
    ExpenseCategoryRead,
    ExpenseCategoryUpdate,
    ExpenseCategoryQuery,
)


def create_expense_category_response_model(expense_category: ExpenseCategorySchema):
    return ExpenseCategoryRead(
        expense_category_id=expense_category.expense_category_id,
        name=expense_category.name,
        description=expense_category.description,
        status=expense_category.status,
        user_id=expense_category.user_id,
    )


def get_all_expense_categories_from_db(
    session: Session,
    token: str,
    limit: int = 10,
    starting_expense_category_id: int = 1,
) -> List[ExpenseCategoryRead]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_categorys = (
        session.query(ExpenseCategorySchema)
        .filter(
            ExpenseCategorySchema.expense_category_id >= starting_expense_category_id
        )
        .order_by(ExpenseCategorySchema.expense_category_id)
        .limit(limit)
        .all()
    )
    expense_category_read_response_models = [
        create_expense_category_response_model(expense_category=expense_category)
        for expense_category in expense_categorys
    ]

    return expense_category_read_response_models


def get_expense_category_from_db(
    session: Session, expense_category_query: ExpenseCategoryQuery
) -> ExpenseCategorySchema | None:
    expense_category_from_db = (
        session.query(ExpenseCategorySchema)
        .filter(ExpenseCategorySchema.name == expense_category_query.name)
        .one_or_none()
    )

    if expense_category_from_db is None:
        return None

    return expense_category_from_db


def create_expense_category_instance(
    session: Session, token: str, new_expense_category: ExpenseCategoryCreate
) -> ExpenseCategoryRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_category_query = ExpenseCategoryQuery(name=new_expense_category.name)

    expense_category_in_db = get_expense_category_from_db(
        session=session, expense_category_query=expense_category_query
    )

    # if package already in the database return a internet package already exists Response
    if expense_category_in_db:
        return AppResponses.expense_category_already_exists()

    new_expense_category_to_insert = ExpenseCategorySchema(
        name=new_expense_category.name,
        description=new_expense_category.description,
        status=new_expense_category.status,
        user_id=new_expense_category.user_id,
    )

    session.add(new_expense_category_to_insert)
    session.commit()
    session.refresh(new_expense_category_to_insert)

    return create_expense_category_response_model(
        expense_category=new_expense_category_to_insert
    )


def update_expense_category_instance(
    session: Session, token: str, expense_category_update: ExpenseCategoryUpdate
) -> ExpenseCategoryRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_category_query = ExpenseCategoryQuery(name=expense_category_update.name)

    expense_category_in_db = get_expense_category_from_db(
        session=session, expense_category_query=expense_category_query
    )

    # if package does not exist in the database return a internet package Not Found Response
    if expense_category_in_db is None:
        return AppResponses.expense_category_not_found()

    new_expense_category_data = expense_category_update.model_dump(exclude_none=True)

    for key, value in new_expense_category_data.items():
        setattr(expense_category_in_db, key, value)

    session.add(expense_category_in_db)
    session.commit()
    session.refresh(expense_category_in_db)

    return create_expense_category_response_model(
        expense_category=expense_category_in_db
    )


def delete_expense_category_instance(
    session: Session, token: str, expense_category_query: ExpenseCategoryQuery
) -> ExpenseCategoryQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_category_in_db = get_expense_category_from_db(
        session=session, expense_category_query=expense_category_query
    )

    # if package does not exist in the database return a internet package Not Found Response
    if expense_category_in_db is None:
        return AppResponses.expense_category_not_found()

    session.delete(expense_category_in_db)
    session.commit()

    return expense_category_query
