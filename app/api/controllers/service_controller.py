from typing import List
from fastapi import Response
from sqlalchemy.orm import Session
from app.api.utils.responses import AppResponses
from app.api.schema.db_schema import ServicesSchema
from app.api.controllers.user_controller import get_user_from_token
from app.api.models.service_model import (
    ServiceCreate,
    ServiceRead,
    ServiceUpdate,
    ServiceQuery,
)


def create_service_response_model(service: ServicesSchema):
    return ServiceRead(
        service_id=service.service_id,
        name=service.name,
        description=service.description,
        status=service.status,
        price=service.price,
        user_id=service.user_id,
    )


def get_all_services_from_db(
    session: Session,
    token: str,
    limit: int = 10,
    starting_service_id: int = 1,
) -> List[ServiceRead]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    services = (
        session.query(ServicesSchema)
        .filter(ServicesSchema.service_id >= starting_service_id)
        .order_by(ServicesSchema.service_id)
        .limit(limit)
        .all()
    )
    service_read_response_models = [
        create_service_response_model(service=service) for service in services
    ]

    return service_read_response_models


def get_service_from_db(
    session: Session, service_query: ServiceQuery
) -> ServicesSchema | None:
    service_from_db = (
        session.query(ServicesSchema)
        .filter(ServicesSchema.name == service_query.name)
        .one_or_none()
    )

    if service_from_db is None:
        return None

    return service_from_db


def create_service_instance(
    session: Session, token: str, new_service: ServiceCreate
) -> ServiceRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    service_query = ServiceQuery(name=new_service.name)

    service_in_db = get_service_from_db(session=session, service_query=service_query)

    # if package already in the database return a service package already exists Response
    if service_in_db:
        return AppResponses.service_already_exists()

    new_service_to_insert = ServicesSchema(
        name=new_service.name,
        description=new_service.description,
        status=new_service.status,
        price=new_service.price,
        user_id=new_service.user_id,
    )

    session.add(new_service_to_insert)
    session.commit()
    session.refresh(new_service_to_insert)

    return create_service_response_model(service=new_service_to_insert)


def update_service_instance(
    session: Session, token: str, service_update: ServiceUpdate
) -> ServiceRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    service_query = ServiceQuery(name=service_update.name)

    service_in_db = get_service_from_db(session=session, service_query=service_query)

    # if package does not exist in the database return a internet package Not Found Response
    if service_in_db is None:
        return AppResponses.service_not_found()

    new_service_data = service_update.model_dump(exclude_none=True)

    for key, value in new_service_data.items():
        setattr(service_in_db, key, value)

    session.add(service_in_db)
    session.commit()
    session.refresh(service_in_db)

    return create_service_response_model(service=service_in_db)


def delete_service_instance(
    session: Session, token: str, service_query: ServiceQuery
) -> ServiceQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    service_in_db = get_service_from_db(session=session, service_query=service_query)

    # if package does not exist in the database return a internet package Not Found Response
    if service_in_db is None:
        return AppResponses.service_not_found()

    session.delete(service_in_db)
    session.commit()

    return service_query
