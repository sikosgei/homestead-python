from pydantic import EmailStr
from jose import jwt, JWTError
from sqlalchemy.orm import Session
from app.core.config import settings
from fastapi import Response, Depends
from app.api.utils.enums import UserRole
from typing import Optional, List, Annotated
from app.api.schema.db_schema import UserSchema
from app.api.utils.responses import AppResponses
from fastapi.security import OAuth2PasswordBearer
from app.api.auth.web_tokens import create_access_token
from app.api.auth.hashing import hash_password, verify_password
from app.api.models.user_model import (
    AccessToken,
    CreateUSer,
    ReadUser,
    UpdateUser,
    UpdateUserPassword,
    UpdateUserStatus,
    UserQuery,
    AuthenticateUser,
)


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


def get_user_from_database(
    session: Session,
    user_name: Optional[EmailStr] = None,
    user_id: Optional[int] = None,
) -> UserSchema | None:
    if user_name and user_id:
        return (
            session.query(UserSchema)
            .filter(UserSchema.user_name == user_name)
            .one_or_none()
        )
    elif user_name:
        return (
            session.query(UserSchema)
            .filter(UserSchema.user_name == user_name)
            .one_or_none()
        )
    elif user_id:
        return (
            session.query(UserSchema)
            .filter(UserSchema.user_id == user_id)
            .one_or_none()
        )
    else:
        return None


def create_user_response_model(user: UserSchema) -> ReadUser:
    return ReadUser(
        user_id=user.user_id,
        user_name=user.user_name,
        full_name=user.full_name,
        permissions_group_id=user.permissions_group_id,
        password=user.password,
        gender=user.gender,
        shares=user.shares,
        balance=user.balance,
        notified=user.notified,
        loan_amount=user.loan_amount,
        primary_phone_number=user.primary_phone_number,
        photo=user.photo,
        address=user.address,
        user_role=user.user_role,
        secondary_phone_number=user.secondary_phone_number,
        national_identification_number=user.national_identification_number,
        account_status=user.account_status,
        permissions=user.permissions.permission_list
    )


def get_users(
    session: Session,
    token: str,
    limit: int = 10,
    starting_user_id: int = 1,
) -> List[ReadUser]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    db_results = (
        session.query(UserSchema)
        .filter(UserSchema.user_id >= starting_user_id)
        .order_by(UserSchema.user_id)
        .limit(limit)
        .all()
    )

    user_read_response_models = [
        create_user_response_model(user=new_user) for new_user in db_results
    ]

    return user_read_response_models


def get_user_instance(
    session: Session, user_query: UserQuery, token: str
) -> ReadUser | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    user_from_db = get_user_from_database(
        session=session, user_name=user_query.user_name
    )
    if user_from_db:
        user_response_model = create_user_response_model(user=user_from_db)
        return user_response_model
    else:
        return AppResponses.user_not_found()


def create_user(
    session: Session, new_user: CreateUSer, token: str
) -> Response | ReadUser:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    if get_user_from_database(session, user_name=new_user.user_name):
        return AppResponses.user_already_exists()
    else:
        # hash password first
        hashed_password = hash_password(password=new_user.password)

        # create new user
        new_user_in_db = UserSchema(
            user_name=new_user.user_name,
            full_name=new_user.full_name,
            permissions_group_id=new_user.permissions_group_id,
            password=hashed_password,
            gender=new_user.gender,
            shares=new_user.shares,
            balance=new_user.balance,
            notified=new_user.notified,
            loan_amount=new_user.loan_amount,
            primary_phone_number=new_user.primary_phone_number,
            photo=new_user.photo,
            address=new_user.address,
            user_role=new_user.user_role,
            secondary_phone_number=new_user.secondary_phone_number,
            national_identification_number=new_user.national_identification_number,
            account_status=new_user.account_status,
        )

        session.add(new_user_in_db)
        session.commit()
        session.refresh(new_user_in_db)

        # create a response model and return it
        new_user_response_model = create_user_response_model(user=new_user_in_db)
        return new_user_response_model


def update_user_password(
    session: Session, new_user_password: UpdateUserPassword, token: str
) -> UpdateUserPassword | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    user_from_db = get_user_from_database(
        session=session, user_name=new_user_password.user_name
    )
    if user_from_db:
        new_password_hash = hash_password(password=new_user_password.password)

        user_from_db.password = new_password_hash

        session.add(user_from_db)
        session.commit()
        session.refresh(user_from_db)

        return new_user_password

    else:
        return AppResponses.user_not_found()


def update_user_status(
    session: Session, new_user_status: UpdateUserStatus, token: str
) -> UpdateUserStatus | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    user_from_db = get_user_from_database(
        session=session, user_name=new_user_status.user_name
    )
    if user_from_db:
        user_from_db.account_status = new_user_status.account_status

        session.add(user_from_db)
        session.commit()
        session.refresh(user_from_db)

        return new_user_status

    else:
        return AppResponses.user_not_found()


def update_user_instance(
    session: Session, user_update: UpdateUser, token: str
) -> ReadUser | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    user_from_db = get_user_from_database(
        session=session, user_name=user_update.user_name
    )
    if user_from_db:
        # Check if password has changed and if it has create hash it
        if user_update.password == user_from_db.password:
            password_to_commit = user_update.password
        else:
            password_to_commit = hash_password(password=user_update.password)

        new_user_data = user_update.model_dump(exclude_none=True)
        for key, value in new_user_data.items():
            setattr(user_from_db, key, value)

        user_from_db.password = password_to_commit

        session.add(user_from_db)
        session.commit()
        session.refresh(user_from_db)

        # create a response model and return it
        new_user_response_model = create_user_response_model(user=user_from_db)
        return new_user_response_model

    else:
        return AppResponses.user_not_found()


def delete_user_instance(
    session: Session, user_query: UserQuery, token: str
) -> UserQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    user_from_db = get_user_from_database(
        session=session, user_name=user_query.user_name
    )

    if user_from_db:
        session.delete(user_from_db)
        session.commit()

        return user_query
    else:
        return AppResponses.user_not_found()


def get_authenticated_user(
    session: Session, user_name: str, password: str
) -> UserSchema | Response:
    user_from_db = get_user_from_database(session=session, user_name=user_name)

    ## check user exists
    if not user_from_db:
        return AppResponses.incorrect_user_details()

    # authenticate user password
    if not verify_password(
        plain_password=password, hashed_password=user_from_db.password
    ):
        return AppResponses.incorrect_user_details()

    # check user's authorization
    if not user_from_db.user_role == UserRole.ADMIN.value:
        return AppResponses.user_not_authorized()

    return user_from_db


def generate_user_token(
    session: Session,
    authenticate_user: AuthenticateUser,
) -> AccessToken | Response:
    user_from_db = get_authenticated_user(
        session=session,
        user_name=authenticate_user.user_name,
        password=authenticate_user.password,
    )

    if user_from_db:
        # create access token
        user_details_dictionary = authenticate_user.model_dump()
        access_token = create_access_token(data=user_details_dictionary)

        return AccessToken(access_token=access_token)
    else:
        return AppResponses.credentials_validation_error()


def get_user_from_token(token: str, session: Session) -> UserSchema | Response:
    try:
        payload = jwt.decode(
            token, settings.secret_key, algorithms=[settings.algorithm]
        )
        user_name = payload.get("user_name", None)
        password = payload.get("password", None)

        user_from_db = get_authenticated_user(
            session=session, user_name=user_name, password=password
        )
        return user_from_db
    except JWTError:
        return AppResponses.credentials_validation_error
