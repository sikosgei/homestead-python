from typing import List
from fastapi import Response
from sqlalchemy.orm import Session
from app.api.utils.responses import AppResponses
from app.api.schema.db_schema import VendorsSchema
from app.api.controllers.user_controller import get_user_from_token
from app.api.models.vendor_model import (
    VendorCreate,
    VendorRead,
    VendorUpdate,
    VendorQuery,
)


def create_vendor_response_model(vendor: VendorsSchema):
    return VendorRead(
        email=vendor.email,
        name=vendor.name,
        description=vendor.description,
        phone_number=vendor.phone_number,
        payment_method=vendor.payment_method,
        account_name=vendor.account_name,
        account_number=vendor.account_number,
        status=vendor.status,
        user_id=vendor.user_id,
        vendor_id=vendor.vendor_id,
    )


def get_all_vendors_from_db(
    session: Session,
    token: str,
    limit: int = 10,
    starting_vendor_id: int = 1,
) -> List[VendorRead]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    vendors = (
        session.query(VendorsSchema)
        .filter(VendorsSchema.vendor_id >= starting_vendor_id)
        .order_by(VendorsSchema.vendor_id)
        .limit(limit)
        .all()
    )
    vendor_read_response_models = [
        create_vendor_response_model(vendor=vendor) for vendor in vendors
    ]

    return vendor_read_response_models


def get_vendor_from_db(
    session: Session, vendor_query: VendorQuery
) -> VendorsSchema | None:
    vendor_from_db = (
        session.query(VendorsSchema)
        .filter(VendorsSchema.email == vendor_query.email)
        .one_or_none()
    )

    if vendor_from_db is None:
        return None

    return vendor_from_db


def create_vendor_instance(
    session: Session, token: str, new_vendor: VendorCreate
) -> VendorRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    vendor_query = VendorQuery(email=new_vendor.email)

    vendor_in_db = get_vendor_from_db(session=session, vendor_query=vendor_query)

    # if package already in the database return a vendor package already exists Response
    if vendor_in_db:
        return AppResponses.vendor_already_exists()

    new_vendor_to_insert = VendorsSchema(
        email=new_vendor.email,
        name=new_vendor.name,
        description=new_vendor.description,
        phone_number=new_vendor.phone_number,
        payment_method=new_vendor.payment_method,
        account_name=new_vendor.account_name,
        account_number=new_vendor.account_number,
        user_id=new_vendor.user_id,
        status=new_vendor.status,
    )

    session.add(new_vendor_to_insert)
    session.commit()
    session.refresh(new_vendor_to_insert)

    return create_vendor_response_model(vendor=new_vendor_to_insert)


def update_vendor_instance(
    session: Session, token: str, vendor_update: VendorUpdate
) -> VendorRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    vendor_query = VendorQuery(email=vendor_update.email)

    vendor_in_db = get_vendor_from_db(session=session, vendor_query=vendor_query)

    # if package does not exist in the database return a internet package Not Found Response
    if vendor_in_db is None:
        return AppResponses.vendor_not_found()

    new_vendor_data = vendor_update.model_dump(exclude_none=True)

    for key, value in new_vendor_data.items():
        setattr(vendor_in_db, key, value)

    session.add(vendor_in_db)
    session.commit()
    session.refresh(vendor_in_db)

    return create_vendor_response_model(vendor=vendor_in_db)


def delete_vendor_instance(
    session: Session, token: str, vendor_query: VendorQuery
) -> VendorQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    vendor_in_db = get_vendor_from_db(session=session, vendor_query=vendor_query)

    # if package does not exist in the database return a internet package Not Found Response
    if vendor_in_db is None:
        return AppResponses.vendor_not_found()

    session.delete(vendor_in_db)
    session.commit()

    return vendor_query
