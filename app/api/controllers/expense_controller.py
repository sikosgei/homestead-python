from typing import List
from fastapi import Response
from sqlalchemy.orm import Session
from app.api.utils.responses import AppResponses
from app.api.schema.db_schema import ExpensesSchema
from app.api.controllers.user_controller import get_user_from_token
from app.api.models.expense_model import (
    ExpenseCreate,
    ExpenseRead,
    ExpenseUpdate,
    ExpenseQuery,
)


def create_expense_response_model(expense: ExpensesSchema):
    return ExpenseRead(
        expense_id=expense.expense_id,
        name=expense.name,
        description=expense.description,
        amount_due=expense.amount_due,
        amount_paid=expense.amount_paid,
        date_incurred=expense.date_incurred,
        status=expense.status,
        user_id=expense.user_id,
        vendor_id=expense.vendor_id,
        expense_category_id=expense.expense_category_id,
    )


def get_all_expenses_from_db(
    session: Session,
    token: str,
    limit: int = 10,
    starting_expense_id: int = 1,
) -> List[ExpenseRead]:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expenses = (
        session.query(ExpensesSchema)
        .filter(ExpensesSchema.expense_id >= starting_expense_id)
        .order_by(ExpensesSchema.expense_id)
        .limit(limit)
        .all()
    )
    expense_read_response_models = [
        create_expense_response_model(expense=expense) for expense in expenses
    ]

    return expense_read_response_models


def get_expense_from_db(
    session: Session, expense_query: ExpenseQuery
) -> ExpensesSchema | None:
    expense_from_db = (
        session.query(ExpensesSchema)
        .filter(ExpensesSchema.name == expense_query.name)
        .one_or_none()
    )

    if expense_from_db is None:
        return None

    return expense_from_db


def create_expense_instance(
    session: Session, token: str, new_expense: ExpenseCreate
) -> ExpenseRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_query = ExpenseQuery(name=new_expense.name)

    expense_in_db = get_expense_from_db(session=session, expense_query=expense_query)

    # if package already in the database return a internet package already exists Response
    if expense_in_db:
        return AppResponses.expense_already_exists()

    new_expense_to_insert = ExpensesSchema(
        name=new_expense.name,
        description=new_expense.description,
        amount_due=new_expense.amount_due,
        amount_paid=new_expense.amount_paid,
        date_incurred=new_expense.date_incurred,
        status=new_expense.status,
        user_id=new_expense.user_id,
        vendor_id=new_expense.vendor_id,
        expense_category_id=new_expense.expense_category_id,
    )

    session.add(new_expense_to_insert)
    session.commit()
    session.refresh(new_expense_to_insert)

    return create_expense_response_model(expense=new_expense_to_insert)


def update_expense_instance(
    session: Session, token: str, expense_update: ExpenseUpdate
) -> ExpenseRead | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_query = ExpenseQuery(name=expense_update.name)

    expense_in_db = get_expense_from_db(session=session, expense_query=expense_query)

    # if package does not exist in the database return a internet package Not Found Response
    if expense_in_db is None:
        return AppResponses.expense_not_found()

    new_expense_data = expense_update.model_dump(exclude_none=True)

    for key, value in new_expense_data.items():
        setattr(expense_in_db, key, value)

    session.add(expense_in_db)
    session.commit()
    session.refresh(expense_in_db)

    return create_expense_response_model(expense=expense_in_db)


def delete_expense_instance(
    session: Session, token: str, expense_query: ExpenseQuery
) -> ExpenseQuery | Response:
    authenticated_user = get_user_from_token(token=token, session=session)

    # check if user is authenticated
    if not authenticated_user:
        return AppResponses.user_not_authorized()

    expense_in_db = get_expense_from_db(session=session, expense_query=expense_query)

    # if package does not exist in the database return a internet package Not Found Response
    if expense_in_db is None:
        return AppResponses.expense_not_found()

    session.delete(expense_in_db)
    session.commit()

    return expense_query
