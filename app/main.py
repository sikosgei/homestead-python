from typing import Annotated
from sqlalchemy.orm import Session
from fastapi import FastAPI, Depends
from app.core.config import settings
from app.api.router import api_router
from contextlib import asynccontextmanager
from app.core.database import initialize_db
from app.core.database import get_database_session
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm
from app.api.models.user_model import AuthenticateUser
from app.api.controllers.user_controller import generate_user_token
from app.api.helpers.initialize_super_user import super_user_initialization


@asynccontextmanager
async def lifespan(app: FastAPI):
    if settings.initialize_database_schema:
        initialize_db()
        super_user_initialization()
        yield


app = FastAPI(
    title=settings.app_name,
    description=settings.app_description,
    version=settings.app_version,
    lifespan=lifespan,
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=settings.allowed_origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(api_router)

@app.post(path="/token")
async def login(
    *,
    session: Session = Depends(get_database_session),
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()]
):
    # print(f"{form_data.username=} {form_data.password=}")
    authenticate_user = AuthenticateUser(
        user_name=form_data.username, password=form_data.password
    )
    
    # print(f"{authenticate_user=}")

    return generate_user_token(
        session=session,
        authenticate_user=authenticate_user,
    )

@app.get("/healthcheck", tags=["Home"])
def health_check():
    return {"status": "OK"}
