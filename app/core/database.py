from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from app.core.config import settings
from app.api.schema.db_schema import Base


engine = create_engine(settings.db_connection_url, echo=False)


def initialize_db():
    Base.metadata.create_all(bind=engine)


def get_database_session():
    with Session(engine) as session:
        yield session
