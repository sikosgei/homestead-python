from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    app_name: str
    app_description: str
    app_version: str
    db_connection_url: str
    initialize_database_schema: bool = True
    allowed_origins: list[str] = [
        "*",
    ]
    secret_key: str
    algorithm: str
    access_token_expire_minutes: int 
    super_user_username: str
    super_user_password: str

    model_config = SettingsConfigDict(env_file=".env")


settings = Settings()
